import AbsPageComponents.BankComponent;
import AbsPageComponents.CardsComponent;
import AbsPageComponents.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MainPage;

import java.time.Duration;
import java.util.Set;

public class BankPageTest {
    private WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor js;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        JavascriptExecutor js = (JavascriptExecutor) driver;

    }

    @Test
    public void checkCardHeader() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseBank();
        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle, "Bank 24/7");
        new BankComponent(driver)
                .chooseMobilBank();

    }

    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}

