package AbsPageComponents;

import org.openqa.selenium.WebDriver;
import pageobject.AbsPageObject;

public abstract class AbsPageComponent extends AbsPageObject {
    public AbsPageComponent(WebDriver driver){
        super(driver);
    }

}

