package AbsPageComponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BankPage;
import pages.CardPage;

public class MainHeaderComponent extends AbsPageComponent{
    public MainHeaderComponent (WebDriver driver){
        super(driver);
    }
    @FindBy(css = "li>span>a[href=\"https://abb-bank.az/az/ferdi/kartlar\"]")
    private WebElement card;

    public CardPage chooseCard(){
        card.click();
        return new CardPage(driver);
    }

    @FindBy(css = "li>span>a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"]")
    private WebElement bank;

    public BankPage chooseBank(){
        bank.click();
        return new BankPage(driver);
    }


}

