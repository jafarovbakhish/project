package AbsPageComponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BankPage;
import pages.CardPage;

public class BankComponent extends AbsPageComponent{
    public BankComponent (WebDriver driver) { super(driver);}
    @FindBy(css = "#js-hover-link > span:nth-child(1) > a")
    private WebElement Mobilbank;

    public BankPage chooseMobilBank(){
        Mobilbank.click();
        return new BankPage(driver);
    }
}


