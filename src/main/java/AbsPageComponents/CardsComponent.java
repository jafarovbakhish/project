package AbsPageComponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.CardPage;

public class CardsComponent extends AbsPageComponent{
    public CardsComponent (WebDriver driver){
        super(driver);
    }
    @FindBy(css = "div.p-2.h-100p.d-flex.flex-column.align-items-start>a>figure>img[alt='TamKart MasterCard Standard PayPass - Debet']")
    private WebElement Mastercard;

    public CardPage chooseCard(){
        Mastercard.click();
        return new CardPage(driver);
    }
}


